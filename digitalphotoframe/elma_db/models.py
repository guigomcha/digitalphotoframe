from django.db import models
from django.utils.translation import gettext_lazy as _


class WatcherStatus(models.Model):
    STATUS = (
        ('frozen', _('Watcher registered and waiting for the first monitored instance')),
        ('pending', _('Watcher ready to be picked up by the WatcherManger to be scheduled')),
        ('running', _('Watcher scheduled and running')),
        ('stop', _('Some event occurred and is to be stopped')),
        ('error', _('Unknown error occurred inside the watcher')),
    )
    status = models.CharField(
        max_length=32,
        choices=STATUS,
        default='frozen',
    )

    def __str__(self):
        return f"Watcher is {self.status}"


class WatcherType(models.Model):
    TYPE = (
        ('time_of_use', _('Watcher based on time as metric to trigger actions')),
        ('users', _('Watcher based on users as metric to trigger actions')),
    )
    type = models.CharField(
        max_length=32,
        choices=TYPE,
        default='time_of_use',
    )

    def __str__(self):
        return f"Watcher type is {self.type}"


class Product(models.Model):
    po_id = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.po_id


class POPrice(models.Model):
    pop_id = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.pop_id


class Operation(models.Model):
    operation_id = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.operation_id


class WatcherInfo(models.Model):
    watcher_id = models.CharField(max_length=200, unique=True)
    type = models.ForeignKey(WatcherType, on_delete=models.CASCADE)
    status = models.ForeignKey(WatcherStatus, on_delete=models.CASCADE)
    operation_id = models.ForeignKey(Operation, on_delete=models.CASCADE)
    po_id = models.ForeignKey(Product, on_delete=models.CASCADE)
    pop_id = models.ForeignKey(POPrice, on_delete=models.CASCADE)
    time_start = models.DateTimeField('Date created')
    time_end = models.DateTimeField('Date deleted')


class Action(models.Model):
    action_id = models.CharField(max_length=200, unique=True)
    watcher_id = models.ForeignKey(WatcherInfo, on_delete=models.CASCADE)
    watcher_type = models.ForeignKey(WatcherType, on_delete=models.CASCADE)
    operation_id = models.ForeignKey(Operation, on_delete=models.CASCADE)
    value = models.FloatField()
    end_trigger = models.CharField(max_length=200)
    time_start = models.DateTimeField('Date created')
    time_end = models.DateTimeField('Date deleted')

"""
Actions

| watcher_id | watcher_type | instance_id | metric_value | time_start | time_end | endTrigger | watcher_type | createdAt | status |
| --- | --- | --- | --- | --- | --- | --- | --- | --- |
| | | | | | | | | |

1.
Watchers
info

| operation_id | po_id | pop_id | vnfd_id | nsd_id | watcher_type | watcher_type | time_start | time_end | status |
| --- | --- | --- | --- | --- | --- | --- | --- | --- |
| | | | | | | | | |

2.
POP
data

| operation_id | po_id | po_payload | pop_id | pop_payload | validFrom | validTo |
| --- | --- | --- | --- | --- | --- | --- |
| | | base64 | | base64 | | |

3.


"""