[[_TOC_]]

# DigitalPhotoFrame

Django-based digital photo frame to store and organize a google drive shared folder
# Run locally in raspberry pi 

```
docker run -p 3306:3306 -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=elma_db -d hypriot/rpi-mysql

```
## First Version

Testing the django framework for a similar problem that needs to be addressed at work

### Scenario:
1. A watcher is a monitoring process that has a single Price Logic (POP) which applies to many descriptors and every instance related directly to them.
2. An instance is the actual deployment of a descriptor. This descriptors come in NSd and VNFd form.
3. Each NSd may contain N VNFd which not all of them are attached to the same POP.
4. An instance may be found not valid when it does not come from a registration message or when it is not the result of a scaling action over the original instance
4.1 A scaling event will be informed
5. Registration messages only cover NSi 
6. POP only cover Descriptor ids
7. Each instance should persist a history of metrics that will at some point be sent to an external service in the form of an Action
8. An Action must contain the full information required to understand what, when, where, how and by whom.



### Ideas

1. Mysql database 
2. Metrics in a row should be limited to a single value with a clear time_start and time_end
3. Subprocess for polling OSM?
4. Correct map from main NSi instance to main VNFi instance?


### Tables

1. Watchers info

| operation_id  | po_id  | pop_id  | vnfd_id  | nsd_id  | watcher_type | watcher_type | time_start | time_end | status | 
|---|---|---|---|---|---|---|---|---|
|   |   |   |   |   |   |   |   |   |
  
2. POP data

| operation_id  | po_id  | po_payload  | pop_id  | pop_payload |  validFrom | validTo |
|---|---|---|---|---|---|---|
|   |   | base64  |   | base64  |   |   |

3. Actions

| watcher_id  | watcher_type  | instance_id | metric_value  | time_start  | time_end  | endTrigger | watcher_type | createdAt | status | 
|---|---|---|---|---|---|---|---|---|
|   |   |   |   |   |   |   |   |   |

4. Instances ???

| descriptor_id  | nfv_level  | isMainInstance | main_instance_id  | instance_id | time_start  | time_end  | endTrigger | 
|---|---|---|---|---|---|---|---|---|
|   |   |   |   |   |   |   |   |   |

5. MainInstances ??

| descriptor_id  | nfv_level  | main_ns_instance_id  | main_vnf_instance_id | time_start  | time_end  | endTrigger | 
|---|---|---|---|---|---|---|---|---|
|   |   |   |   |   |   |   |   |   |

6. Descriptors ??

| descriptor_id  | nfv_level  |  isMainInstance | main_vnf_instance_id | time_start  | time_end  | endTrigger | 
|---|---|---|---|---|---|---|---|---|
|   |   |   |   |   |   |   |   |   |

7. Watcher status : frozen, pending, running, stop, error
| status  | description  |  
|---|---|
|   |   |

8. nfv levels : VNF, NS
| status  | description  |  
|---|---|
|   |   |